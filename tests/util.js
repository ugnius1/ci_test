
var mongoose = require('mongoose')

var config = require('../config')
config.mongoConnection = config.mongoConnectionForTests

function reconnectMongoose(callback) {
	mongoose.connection.close(function () {
		mongoose.models = {}
		mongoose.modelSchemas = {}
		callback()
	})
}


function removeDataFromMongo(callback) {
	if (!mongoose.connection.name.includes('testing')) {
		return callback(new Error(`Will not delete data from non testing DB ${mongoose.connection.name}`))
	}
	Promise.all([
		require('../models/Product').remove()
	]).then(() => callback(), callback)
}


module.exports = {
	reconnectMongoose: reconnectMongoose,
	removeDataFromMongo: removeDataFromMongo,
}
