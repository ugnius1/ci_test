var assert = require('assert')
assert.same = assert.deepEqual
var request = require('supertest')
var util = require('../util')

describe('/products', function () {

	var app
	var Product

	before(function (done) {
		util.reconnectMongoose((err) => {
			if (err) { return done(err) }
			app = require('../../app')
			Product = require('../../models/Product')
			done()
		})
	})

	beforeEach(util.removeDataFromMongo)


	describe('GET /products', function () {
		it('should return empty json list', function (done) {
			request(app)
				.get('/products')
				.expect('Content-Type', /json/)
				.expect(200,
				[],
				done)
		})
	})


	describe('GET /products', function () {
		it('should return product list', function (done) {

			Promise.all([
				Product.create({ _id: '000000000000000000000001', name: 'banana' }),
				Product.create({ _id: '000000000000000000000002', name: 'lemon' }),
			]).then(() => {
				request(app)
					.get('/products')
					.expect(200,
					[
						{ _id: '000000000000000000000001', name: 'banana' },
						{ _id: '000000000000000000000002', name: 'lemon' },
					],
					done)
			}, done)
		})
	})

	describe('POST /products', function () {
		it('should save product', function (done) {

			request(app)
				.post('/products')
				.send({ name: 'lemon' })
				.expect(201, function (err) {
					if (err) return done(err)

					Product.find({}).lean().select('-_id').exec(function(err, products) {
						if (err) return done(err)

						assert.same([{ name: 'lemon'}], products)

						done()
					})
				})
		})
	})


})




