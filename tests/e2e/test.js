
var assert = require('assert')
assert.same = assert.deepEqual
var util = require('../util')
var webdriver = require('selenium-webdriver')
const By = webdriver.By
var assertPixels = require('mocha-pixelp-reporter').assertPixels

describe('e2e', function () {

	this.timeout(30 * 1000)
	this.slow(1000)
	var server, driver
	
	var port = ((Math.random() * 1000) | 0) + 9000
	var testHost = process.env.TEST_HOST || '10.0.75.1'
	var url = 'http://' + testHost + ':' + port
	var browser = process.env.BROWSER || 'chrome'
	var seleniumRemoteUrl = process.env.SELENIUM_REMOTE_URL || 'http://localhost:4444/wd/hub'

	function enableDestroy(server) {
		var connections = {}

		server.on('connection', function(conn) {
			var key = conn.remoteAddress + ':' + conn.remotePort;
			connections[key] = conn;
			conn.on('close', function() {
				delete connections[key];
			});
		});

		server.destroy = function(cb) {
			server.close(cb);
			for (var key in connections)
				connections[key].destroy();
		};
	}

	before(function (done) {
		driver = new webdriver.Builder()
			.forBrowser(browser)
			.usingServer(seleniumRemoteUrl)
			.build()
		server = require('../../app').listen(port, done)
		enableDestroy(server)
	})

	before(function() {
		return driver.manage().window().setSize( 1200, 800 )
	})

	after(() => driver.quit())
	after(util.reconnectMongoose)
	after((done) => server.destroy(done))


	it('should have correct title', function(done) {
		driver.navigate().to(url)
		driver.getTitle()
			.then(value => assert.same('pageTitle', value))
			.then(() => done())
	})

	it('list should look pretty', function(done) {
		driver.navigate().to(url)
		assertPixels(driver, {
			chrome: 'e8b9fb41ad662ee4e1b5c02ba227f30948e3f680',
			firefox: '39dc418a7cb573c8ff191f5c83d0fbeb83e58554',
		}[browser], By.css('ol'))
			.then(() => done())
	})

})