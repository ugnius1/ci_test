var config = require('./config')
var app = require('./app')

app.listen(config.port, function() {
	console.log('Server listening on: ' + this._connectionKey)
})
