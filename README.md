# README #

project requires config.js with structure
```
#!javascript
module.exports = {
 port: 8081,
 mongoConnectionForTests: 'localhost/ci_testing'
}
```


To run api tests:

```
docker run --rm -d -p 27017:27017 --name mongo mongo:3.2

"./node_modules/.bin/mocha" tests/unit tests/api

docker stop mongo
```


To run e2e tests:

```
docker run --rm -d -p 27017:27017 --name mongo mongo:3.2
docker run --rm -d -p 4444:4444 --name selenium-hub selenium/hub:3.4.0-chromium
docker run --rm -d --name selenium-chrome --link selenium-hub:hub selenium/node-chrome:3.4.0-chromium
docker run --rm -d --name selenium-firefox --link selenium-hub:hub selenium/node-firefox:3.4.0-chromium

"./node_modules/.bin/mocha" tests/e2e -R mocha-pixelp-reporter

docker stop mongo selenium-hub selenium-chrome selenium-firefox

```

