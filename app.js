
var express = require('express')
var mongoose = require('mongoose')
var config = require('./config')
var bodyParser = require('body-parser')
var pug = require('pug');

mongoose.Promise = global.Promise
mongoose.connect(config.mongoConnection)


var app = express()
app.set('view engine', 'pug')

app.use(bodyParser.json())


app.get('/', (req, res, err) => {
	res.render('index')
})

var Product = require('./models/Product')

app.get('/products', (req, res, next) => {

	Product.find({}).lean().select('name').exec((err, products) => {
		if (err) { return next(err) }
		res.json(products)
	})
})

app.post('/products', (req, res, next) => {
	Product.create(req.body, function(err) {
		if (err) return next(err)
		res.status(201).end()
	})
})


module.exports = app
